package quiz;

import java.util.*;

import Questions.Question;
import formations.EtatQuiz;

public class Quiz {
	private String nom;
	private int nbrQuestion;
	private double totalPoints;
	private Date dateOuverture, dateExpiration;
	private TreeMap<Integer, Notion> notionsQuiz;
	//private Map<Notion, ArrayList<Question>> lesQuestions;
		//private Map<Question, Bareme> lebareme;

	public Quiz(String nom, int nbrQuestion, double totalPoints, Date dateOuverture, Date dateExpiration) {
		this.nom = nom;
		this.nbrQuestion = nbrQuestion;
		this.totalPoints = totalPoints;
		this.dateOuverture = dateOuverture;
		this.dateExpiration = dateExpiration;

	}

	public void Create () {
		// cree un notion
	}
	public void AfficherInfo () {
		System.out.println("Quiz de "+ this.nom);
		System.out.println("Nombre de questions "+this.nbrQuestion);
		System.out.println("Nombre totale de points "+this.totalPoints);
		System.out.println("Date d ouverture "+ this.dateOuverture+ " Date d expiration "+ this.dateExpiration);
	   Set<Integer> Nkeys= this.notionsQuiz.keySet();
	   
	   
	   for (Integer id : Nkeys) {
		Notion n = this.notionsQuiz.get(id);
		n.AfficherInfo();
	}
	   
		
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setTotalPoints(double p) {
		this.totalPoints = p;
		// this.calculerBareme();
	}

	public void setNbrQuestion(int NbrQ) {
		this.setNbrQuestion(NbrQ);
	}

	public Date getDateOuverture() {
		return dateOuverture;
	}

	public void setDateOuverture(Date dateOuverture) {
		this.dateOuverture = dateOuverture;
	}

	public Date getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public TreeMap<Integer, Notion> getNotionsQuiz() {
		return notionsQuiz;
	}

	public void setNotionsQuiz(TreeMap<Integer, Notion> notionsQuiz) {
		this.notionsQuiz = notionsQuiz;
	}

	public String getNom() {
		return nom;
	}

	public int getNbrQuestion() {
		return nbrQuestion;
	}

	public double getTotalPoints() {
		return totalPoints;
	}

	public void addNotion(Notion notion) {
		int leng = this.notionsQuiz.size();
		if (this.notionsQuiz.containsValue(notion) == false) {
			this.notionsQuiz.put(leng, notion);
		} else {
			System.out.println("La notion existe d�ja dans cette quiz");
		}

	}

	public void genererQuiz(TreeMap<Integer, Notion> notionsQuiz) {
		// cree les notions
		this.notionsQuiz.clear();
		Set<Integer> keys = notionsQuiz.keySet();
		for (Integer key : keys) {
			this.notionsQuiz.put(key, notionsQuiz.get(key));
		}
	}

	/*
	 * public void calculerBareme() { //pourcentage
	 * 
	 * }
	 * 
	 * public void modifierBareme() { this.calculerBareme(); }
	 */

}
