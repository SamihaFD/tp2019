package comptes;


public class PassIncorrectException extends Exception {

	public String getMessage(){
		return "Mot de passe incorrect, Veuillez introduire un autre";
	}
}
