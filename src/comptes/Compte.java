package comptes;


public abstract class Compte {
 private String login;
 private String password;
 
 public Compte(String user, String pass){
	 this.login=user;
	 this.password=pass;
 }
 
 public boolean authentifier(String pass) throws PassIncorrectException{
	  if (this.password.compareTo(pass)==0) return true;
	  else throw new PassIncorrectException();
 }
}
