package Questions;

public class Proposition {
	private String proposition;
	private String typeQ;
	public String getProposition() {
		return proposition;
	}
	public void setProposition(String proposition) {
		this.proposition = proposition;
	}
	public String getTypeQ() {
		return typeQ;
	}
	public Proposition(String proposition, String typeQ) {
		super();
		this.proposition = proposition;
		this.typeQ = typeQ;
	}
	public void setTypeQ(String typeQ) {
		this.typeQ = typeQ;
	}
}
