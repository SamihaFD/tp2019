package Questions;

import java.util.ArrayList;

public class Reponse {
	private ArrayList<String> Text;
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<String> getText() {
		return Text;
	}

	public void setText(ArrayList<String> text) {
		Text = text;
	}

	public Reponse(ArrayList<String> text, String type) {
		super();
		Text = text;
		this.type = type;
	}

	
}
