package Questions;

import java.util.ArrayList;
import java.util.Scanner;

import quiz.Notion;

public class QCM extends Question {
	private ArrayList<Proposition> proposition;
	private Notion notion;
	private String Texte;
	private Reponse reponseCorrecte;
	private Reponse reponseUnCorrecte;
	
	public QCM(Notion notion) {
		this.notion = notion;
	}

	public void cree() {
		// liste de r�ponse
		Scanner scanner = new Scanner(System.in);
		System.out.println("entrer la question");
		String questionText = scanner.nextLine();
		this.Texte = questionText;

		System.out.println("entrer le nombre de propositions");
		Integer NbrPropositions = scanner.nextInt();
		ArrayList<String> reponsecorrecte = new ArrayList<>();
		ArrayList<String> reponseUncorrecte = new ArrayList<>();
		this.proposition = new ArrayList<>();
		
		for (int i = 0; i < NbrPropositions; i++) {
			System.out.println("entrer la " + i + " proposition");
			String propoText = scanner.nextLine();
			Proposition propo = new Proposition(propoText, "QCM");
			this.proposition.add(propo);
			System.out.println("Cette proposition et une r�ponse oui ou non");
			String repons = scanner.nextLine();
			if (repons.equals("oui")) {
				reponsecorrecte.add(repons);
			}else {
				reponseUncorrecte.add(repons);
			}
		}
		this.reponseCorrecte = new Reponse(reponsecorrecte,"correcte"); // reponse ou propositions
		this.reponseUnCorrecte = new Reponse(reponseUncorrecte,"uncorrecte"); // reponse ou propositions
		
		System.out.println("------------------------------");

	}

	
	public ArrayList<Proposition> getProposition() {
		return proposition;
	}

	public void setProposition(ArrayList<Proposition> proposition) {
		this.proposition = proposition;
	}


	public Reponse getReponseCorrecte() {
		return reponseCorrecte;
	}

	public void setReponseCorrecte(Reponse reponseCorrecte) {
		this.reponseCorrecte = reponseCorrecte;
	}

	public Reponse getReponseUnCorrecte() {
		return reponseUnCorrecte;
	}

	public void setReponseUnCorrecte(Reponse reponseUnCorrecte) {
		this.reponseUnCorrecte = reponseUnCorrecte;
	}

	public Notion getNotion() {
		return notion;
	}

	public void setNotion(Notion notion) {
		this.notion = notion;
	}

	public String getTexte() {
		return Texte;
	}

	public void setTexte(String texte) {
		Texte = texte;
	}

	

}
