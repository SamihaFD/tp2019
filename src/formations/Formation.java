package formations;

import java.util.*;

import comptes.Formateur;
import quiz.Notion;

public class Formation {
	private String nom, description;
	private Formateur leformateur;
	private Date dateDebut, dateFin;
	private Set<Notion> notionsEnseignees;

	public Formation(String nom, String description, Formateur leformateur, Date dateDebut, Date dateFin) {
		Set<Notion> notionsEnseignees = new HashSet<Notion>();
		this.nom = nom;
		this.description = description;
		this.leformateur = leformateur;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public void ajouterNotion(Notion n) {
		// saisir un exemple de question
		//pour chaque question, saisir un ensemble de r�ponse
		//r�ponse compris correcte et un correcte
		
		notionsEnseignees.add(n);
		
	}

	public void supprimerNotion(Notion n) {

	}

	public void modifierNotion(Notion n) {

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Formateur getLeformateur() {
		return leformateur;
	}

	public void setLeformateur(Formateur leformateur) {
		this.leformateur = leformateur;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Set<Notion> getNotionsEnseignees() {
		return notionsEnseignees;
	}

	public void setNotionsEnseignees(Set<Notion> notionsEnseignees) {
		this.notionsEnseignees = notionsEnseignees;
	}

}
